import React, { Component } from 'react';
import { Root } from "native-base";
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {Actions, Scene, Router} from 'react-native-router-flux';
import Login from './app/components/Login';
import Devices from './app/components/Devices';
import Switches from './app/components/Switches';

const scenes = Actions.create(
  <Scene key="root">
    <Scene key="home" component={Login} hideNavBar={true} initial={true}/>
    <Scene key="devices" component={Devices} hideNavBar={true}/>
    <Scene key="switches" component={Switches} hideNavBar={true}/>
  </Scene>
);

export default class App extends Component {
  render() {
      return <Root><Router scenes={scenes}/></Root>
  }
}
