import React, { Component } from 'react';
import { StyleSheet,
  View,
  AsyncStorage,
  TouchableOpacity,
  StatusBar,
  ListView,
  Text,
  Alert,
  } from 'react-native';

import { Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title, } from 'native-base';

import Device from './child/Device';
import { Actions } from 'react-native-router-flux';

export default class Devices extends React.Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state ={
            deviceData: ds,
        }
  }

  componentDidMount() {
    this._loadAllDevices().done();
  }

  _loadAllDevices = async() => {
    var token = await AsyncStorage.getItem('automationApiToken');
    fetch("http://developerhire.com:3000/api/v2/devices", {
      method : "GET",
      headers: {
        'Authorization' : token
      }
    }).then((response) => response.json())
    .then((res) => {
      this.setState({deviceData: this.state.deviceData.cloneWithRows(res.payload)});
    }).catch((err) => {
      console.log(err);
    });
  }

  logout = () => {
    Alert.alert(
      'Confirmation',
      'Are you sure to logout.?',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: async() => {
          await AsyncStorage.removeItem('automationApiToken');
          Actions.home();
        }
      },
      ],
      { cancelable: true }
    );
    //AsyncStorage.removeItem('automationApiToken');
  }

  render() {
    return (
      <Container>
      <Header>
        <Body>
          <Title>Your Devices</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => this.logout()}>
            <Icon name='paw' />
          </Button>
        </Right>
      </Header>
        <View style={styles.DeviceWrapper}>
          <ListView
            dataSource={this.state.deviceData}
            renderRow={(data) => <Device {...data} />}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  DeviceWrapper: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#F5F5F5',
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
  },
});
