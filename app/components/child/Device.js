import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Button, Icon, Right, H1, } from 'native-base';
import { Actions } from 'react-native-router-flux';

import {
  Alert,
  } from 'react-native';

var loadSwitchView = (deviceKey) => {
    Actions.switches({deviceKey: deviceKey});
}

const Device = (props) => (
        <Content>
          <Card>
            <CardItem header>
              <Left>
                <Body>
                  <H1>{props.devicekey}</H1>
                  <Text note>{props.updatedAt}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  No device details to display yet. However I think this is working fine.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
              <Button small dark>
                <Text>Settings</Text>
              </Button>
              </Left>
              <Right>
                <Button small blue onPress={() => loadSwitchView(props.devicekey)}>
                  <Text>Switches</Text>
                </Button>
              </Right>
            </CardItem>
         </Card>
        </Content>
);



export default Device;
