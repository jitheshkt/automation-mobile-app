import React from 'react';
import { View, Text, StyleSheet,} from 'react-native';

const CustomNavbar = (props) => (
  <View style={styles.deviceWrapper}>
    <Text>
      Hello World
    </Text>
  </View>
);


const styles = StyleSheet.create({
  deviceWrapper: {
    flex:1,
    padding: 12,
    height:250,
    flexDirection: 'row',
    backgroundColor: 'red',
    alignSelf: 'stretch',
    marginBottom: 10,

  }
});

export default CustomNavbar;
