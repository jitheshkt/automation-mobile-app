import React, { Component } from 'react';
import { StyleSheet,
  View,
  Text,
  AsyncStorage,
  ListView,
  } from 'react-native';

import { Actions } from 'react-native-router-flux';
import SingleSwitch from './child/SingleSwitch';

export default class Switches extends React.Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      digitalSwitches: ds,
      singleDevice:[],
    }
  }

  componentDidMount() {
    this._loadAllSwitches().done();
  }

  _loadAllSwitches = async() => {
    var token = await AsyncStorage.getItem('automationApiToken');
    var apiUrl = `http://developerhire.com:3000/api/v2/devices/${this.props.deviceKey}`;
    fetch(apiUrl, {
      method : "GET",
      headers: {
        'Authorization' : token
      }
    }).then((response) => response.json())
    .then((res) => {
      this.setState({digitalSwitches: this.state.digitalSwitches.cloneWithRows(res.payload.digital), singleDevice: res.payload});
    }).catch((err) => {
      console.log(err);
    });
  }

  render() {
    return (
      <ListView
        dataSource={this.state.digitalSwitches}
        renderRow={(data) => <SingleSwitch {...data} />}
      />
    );
  }
}
