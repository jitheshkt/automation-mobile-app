import React, { Component } from 'react';
import { StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  Alert,
  Keyboard,
  } from 'react-native';

import { Actions } from 'react-native-router-flux';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import {Toast} from 'native-base';


export default class Login extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    }
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  _loadInitialState = async() => {
    var token = await AsyncStorage.getItem('automationApiToken');
    if(token !== null) {
      Actions.devices();
    }
  }

  render() {
    return (
        <KeyboardAvoidingView behaviour='padding' style={styles.wrapper}>
          <View style={styles.top}>
          <Text style={styles.logo}>O</Text>
          </View>
          <View style={styles.container}>
            <StatusBar backgroundColor="#29B6F6" barStyle="light-content" />
            <TextInput style={styles.TextInput} placeholder="Email" value={this.props.email} onChangeText={(email) => this.setState({email})}
              underlineColorAndroid="transparent" keyboardType="email-address" underlineColorAndroid="#CFD8DC" onSubmitEditing={() => this.password.focus()} placeholderTextColor="#B0BEC5" />

            <TextInput style={styles.TextInput} placeholder="Password" value={this.props.password} onChangeText={(password) => this.setState({password})}
              underlineColorAndroid="transparent" secureTextEntry={true} underlineColorAndroid="#CFD8DC" placeholderTextColor="#B0BEC5" ref={(input) => this.password = input}/>

              <TouchableOpacity
                style={styles.button}
                onPress={this.login}>
                  <Text style={styles.buttonText}>SIGN IN</Text>
                </TouchableOpacity>

          </View>
        </KeyboardAvoidingView>
      );
  }

  login = () => {
    dismissKeyboard();
    var payload = {
      email : this.state.email,
      password : this.state.password,
    };
    var formBody = [];
    for (var property in payload) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(payload[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    fetch('http://developerhire.com:3000/api/v2/authenticate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formBody
    }).then((response) => response.json())
    .then((res) => {
      if(res.status === true) {
        AsyncStorage.setItem('automationApiToken', res.token);
        Actions.devices();
      } else {
        Toast.show({
                  text: res.message,
                  position: 'bottom',
                  buttonText: 'Okay'
                });
      }
    }).catch((err) => {
      Toast.show({
                text: 'something terribly went wrong',
                position: 'bottom',
                buttonText: 'Okay'
              });
    })
  }

};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  logo: {
    color: '#fff',
    fontSize: 120,
    fontWeight:'900',
    width: '100%',
    textAlign: 'center',
    alignSelf: 'stretch',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5F5F5',
    paddingLeft: 40,
    paddingRight: 40,
  },
  top: {
    height: '45%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#29B6F6',
  },
  header: {
    fontSize: 24,
    marginBottom: 40,
    color: '#fff',
    fontWeight: 'bold',
  },
  TextInput: {
    alignSelf: 'stretch',
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#F5F5F5',
    borderRadius: 25,
  },
  button: {
    width:'100%',
    backgroundColor:'#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
});
